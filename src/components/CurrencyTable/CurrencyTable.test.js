import { shallow } from 'enzyme';
import { Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import { CurrencyTable } from "./CurrencyTable";
import * as useCurrencyValuesComparisonHook from '../../hooks/useCurrencyValuesComparison/useCurrencyValuesComparison';
import { CurrencyTableHead } from '../CurrencyTableHead/CurrencyTableHead';

describe('<CurrencyTable />', () => {
  const currentRates = {
    USD: 25,
    CAD: 8,
    EUR: 20
  };
  const ratePreviousDate = {
    USD: 20,
    CAD: 10,
    EUR: 20
  };
  beforeEach(() => {
    jest.spyOn(useCurrencyValuesComparisonHook, 'useCurrencyValuesComparison').mockReturnValue({
      fullCurrenciesInfo: []
    });
  });
  describe('static data', () => {
    const expectedTableHeaders = ['CURRENCY', 'EXCHANGE RATE', 'CHANGE (%)'];

    it('should show the correct table headers', () => {
      const component = shallow(<CurrencyTable currencyCurrentDate={{}} />);
      const tableHead = component.find(TableHead);
      tableHead.find(TableCell).forEach((cell, index) => {
        expect(cell.text()).toBe(expectedTableHeaders[index]);
      });
    });

    it('should pass the info to the hook', () => {
      shallow(<CurrencyTable currencyCurrentDate={currentRates} currencyPrevDate={ratePreviousDate} />);
      expect(useCurrencyValuesComparisonHook.useCurrencyValuesComparison)
        .toHaveBeenCalledWith(currentRates, ratePreviousDate);
    });
  });

  describe('table data', () => {
    let component;
    let tableBody;
    let fullCurrenciesInfo;
    beforeEach(() => {
      fullCurrenciesInfo = [{
        currency: 'GBP',
        rate: 12,
        variation: 5
      }, {
        currency: 'JPY',
        rate: 1204,
        variation: .5
      }, {
        currency: 'NZD',
        rate: 20,
        variation: -1
      }];
      useCurrencyValuesComparisonHook.useCurrencyValuesComparison.mockReturnValue({
        fullCurrenciesInfo
      });
      component = shallow(<CurrencyTable currencyCurrentDate={currentRates} currencyPrevDate={ratePreviousDate} />);
      tableBody = component.find(TableBody);
    });
    it('should render the correct number of lines', () => {
      expect(tableBody.find(TableRow).length).toBe(3);
    });
    it('should render the correct currencies', () => {
      tableBody.find(TableRow).forEach((tableRow, index) => {
        const selectedResult = fullCurrenciesInfo[index];
        const cells = tableRow.find(TableCell);
        expect(cells.get(0).props.children).toBe(selectedResult.currency);
        expect(cells.get(1).props.children).toBe(selectedResult.rate);
        expect(cells.get(2).props.children).toBe(selectedResult.variation);
      });
    });

    describe('on sort event', () => {
      let currencyTableHead;
      beforeEach(() => {
        currencyTableHead = component.find(CurrencyTableHead);
      });
      it('should revert the sort when currency revert sort is selected', () => {
        currencyTableHead.props().onThClick('currency');
        component.find(TableRow).forEach((row, index) => {
          const selectedResult = fullCurrenciesInfo[fullCurrenciesInfo.length - index - 1];

          const cells = row.find(TableCell);
          expect(cells.get(0).props.children).toBe(selectedResult.currency);
          expect(cells.get(1).props.children).toBe(selectedResult.rate);
          expect(cells.get(2).props.children).toBe(selectedResult.variation);
        });

      });

      it('should sort by rate when rate sort is selected', () => {
        currencyTableHead.props().onThClick('rate');
        const expectedResults = [
          fullCurrenciesInfo[0],
          fullCurrenciesInfo[2],
          fullCurrenciesInfo[1]
        ];
        component.find(TableRow).forEach((row, index) => {
          const selectedResult = expectedResults[index];

          const cells = row.find(TableCell);
          expect(cells.get(0).props.children).toBe(selectedResult.currency);
          expect(cells.get(1).props.children).toBe(selectedResult.rate);
          expect(cells.get(2).props.children).toBe(selectedResult.variation);
        });
      });

      it('should sort by rate revert', () => {
        currencyTableHead.props().onThClick('rate');
        currencyTableHead.props().onThClick('rate');
        const expectedResults = [
          fullCurrenciesInfo[1],
          fullCurrenciesInfo[2],
          fullCurrenciesInfo[0]
        ];
        component.find(TableRow).forEach((row, index) => {
          const selectedResult = expectedResults[index];

          const cells = row.find(TableCell);
          expect(cells.get(0).props.children).toBe(selectedResult.currency);
          expect(cells.get(1).props.children).toBe(selectedResult.rate);
          expect(cells.get(2).props.children).toBe(selectedResult.variation);
        });
      });

      it('should sort by variation', () => {
        currencyTableHead.props().onThClick('variation');
        const expectedResults = [
          fullCurrenciesInfo[2],
          fullCurrenciesInfo[1],
          fullCurrenciesInfo[0]
        ];
        component.find(TableRow).forEach((row, index) => {
          const selectedResult = expectedResults[index];

          const cells = row.find(TableCell);
          expect(cells.get(0).props.children).toBe(selectedResult.currency);
          expect(cells.get(1).props.children).toBe(selectedResult.rate);
          expect(cells.get(2).props.children).toBe(selectedResult.variation);
        });
      });

      it('should sort by variation revert', () => {
        currencyTableHead.props().onThClick('variation');
        currencyTableHead.props().onThClick('variation');
        const expectedResults = [
          fullCurrenciesInfo[0],
          fullCurrenciesInfo[1],
          fullCurrenciesInfo[2]
        ];
        component.find(TableRow).forEach((row, index) => {
          const selectedResult = expectedResults[index];

          const cells = row.find(TableCell);
          expect(cells.get(0).props.children).toBe(selectedResult.currency);
          expect(cells.get(1).props.children).toBe(selectedResult.rate);
          expect(cells.get(2).props.children).toBe(selectedResult.variation);
        });
      });
    });

    describe('low and high variation highlight', () => {
      let component;
      let tableBody;
      let fullCurrenciesInfo;
      beforeEach(() => {
        fullCurrenciesInfo = [{
          currency: 'GBP',
          rate: 12,
          variation: 5
        }, {
          currency: 'JPY',
          rate: 1204,
          variation: .5
        }, {
          currency: 'NZD',
          rate: 20,
          variation: -1
        }];
        useCurrencyValuesComparisonHook.useCurrencyValuesComparison.mockReturnValue({
          fullCurrenciesInfo
        });
        component = shallow(<CurrencyTable currencyCurrentDate={currentRates} currencyPrevDate={ratePreviousDate} />);
        tableBody = component.find(TableBody);
      });
      it('should highlight most positive variation', () => {
        expect(tableBody.find('.row--GBP').props().className).toContain('high-variation');
      });

      it('should highlight most negative variation', () => {
        expect(tableBody.find('.row--NZD').props().className).toContain('low-variation');
      });
    });
  });
});