import { useState, useMemo } from 'react';
import { Table, TableBody, TableRow, TableCell } from '@mui/material';
import { useCurrencyValuesComparison } from '../../hooks/useCurrencyValuesComparison/useCurrencyValuesComparison';
import { CurrencyTableHead } from '../CurrencyTableHead/CurrencyTableHead';
import './CurrencyTable.css';

export function CurrencyTable(props) {
  const [sortInfo, setSortInfo] = useState({
    key: 'currency',
    reverted: false
  })
  const { currencyPrevDate, currencyCurrentDate } = props;
  const { fullCurrenciesInfo } = useCurrencyValuesComparison(currencyCurrentDate, currencyPrevDate);

  function updateSortInfo(key) {
    setSortInfo(oldSortInfo => ({
      key,
      reverted: key === oldSortInfo.key && !oldSortInfo.reverted
    }));
  }

  const sortedCurrenciesInfo = useMemo(() => {
    return [...fullCurrenciesInfo].sort((cur1, cur2) => {
      const value1 = cur1[sortInfo.key];
      const value2 = cur2[sortInfo.key];
      const originalSort = value1 > value2 ? -1 : 1;
      return sortInfo.reverted ? originalSort : -originalSort;
    });
  }, [sortInfo, fullCurrenciesInfo]);

  const { highVariation, lowVariation } = useMemo(() => {
    if (!currencyPrevDate) return {};
    const sortedCurrencies = [...fullCurrenciesInfo]
      .sort((cur1, cur2) => cur1.variation - cur2.variation);
    return {
      lowVariation: sortedCurrencies[0],
      highVariation: sortedCurrencies[sortedCurrencies.length - 1]
    };
  }, [fullCurrenciesInfo]);

  function getRowClass(rowCurrency) {
    switch (rowCurrency) {
      case highVariation?.currency:
        return 'high-variation';
      case lowVariation?.currency:
        return 'low-variation';
      default:
        return '';
    }
  }

  return <div>
    <Table stickyHeader>
      <CurrencyTableHead sortInfo={sortInfo} onThClick={updateSortInfo} />
      <TableBody>
        {sortedCurrenciesInfo.map(currencyInfo => {
          const rowClass = getRowClass(currencyInfo.currency);

          return <TableRow key={currencyInfo.currency} className={`currency-table__body__row ${rowClass} row--${currencyInfo.currency}`}>
            <TableCell>{currencyInfo.currency}</TableCell>
            <TableCell>{currencyInfo.rate}</TableCell>
            <TableCell>{currencyInfo.variation}</TableCell>
          </TableRow>;
        })}
      </TableBody>
    </Table>
  </div>;
}