import { useMemo } from 'react';
import { TableHead, TableRow, TableCell } from '@mui/material';
import './currencyTableHead.css';

export function CurrencyTableHead(props) {
  const { onThClick, sortInfo } = props;
  const {
    thCurrencyClassName,
    thRateClassName,
    thVariationClassName
  } = useMemo(() => {
    return {
      thCurrencyClassName: getClassName('currency'),
      thRateClassName: getClassName('rate'),
      thVariationClassName: getClassName('variation'),
    };

    function getClassName(fieldName) {
      if (fieldName !== sortInfo.key) return '';
      return sortInfo.reverted ? 'th-down' : 'th-up';
    }
  }, [sortInfo]);
  return <TableHead>
    <TableRow>
      <TableCell className={`currency-table-head__th currency-table-head__th-currency ${thCurrencyClassName}`} onClick={() => onThClick('currency')}>CURRENCY</TableCell>
      <TableCell className={`currency-table-head__th currency-table-head__th-rate ${thRateClassName}`} onClick={() => onThClick('rate')}>EXCHANGE RATE</TableCell>
      <TableCell className={`currency-table-head__th currency-table-head__th-variation ${thVariationClassName}`} onClick={() => onThClick('variation')}>CHANGE (%)</TableCell>
    </TableRow>
  </TableHead>;
}