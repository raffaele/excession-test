import { shallow } from 'enzyme';
import { Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import { CurrencyTableHead } from "./CurrencyTableHead";


describe('<CurrencyTableHead />', () => {
  let component;
  let sortInfo;
  beforeEach(() => {
    sortInfo = jest.fn();
  });
  describe('layout', () => {
    it('should select currency th if reported in sortInfo', () => {
      component = shallow(<CurrencyTableHead sortInfo={{ key: 'currency', reverted: false }} onThClick={() => { }} />);
      const cells = component.find(TableCell);
      expect(cells.get(0).props.className).toContain('th-up');
      expect(cells.get(0).props.className).not.toContain('th-down');
      expect(cells.get(1).props.className).not.toContain('th-up');
      expect(cells.get(1).props.className).not.toContain('th-down');
      expect(cells.get(2).props.className).not.toContain('th-up');
      expect(cells.get(2).props.className).not.toContain('th-down');
    });
    it('should select revert currency th if reported in sortInfo', () => {
      component = shallow(<CurrencyTableHead sortInfo={{ key: 'currency', reverted: true }} onThClick={() => { }} />);
      const cells = component.find(TableCell);
      expect(cells.get(0).props.className).not.toContain('th-up');
      expect(cells.get(0).props.className).toContain('th-down');
      expect(cells.get(1).props.className).not.toContain('th-up');
      expect(cells.get(1).props.className).not.toContain('th-down');
      expect(cells.get(2).props.className).not.toContain('th-up');
      expect(cells.get(2).props.className).not.toContain('th-down');
    });

    it('should select rate th if reported in sortInfo', () => {
      component = shallow(<CurrencyTableHead sortInfo={{ key: 'rate', reverted: false }} onThClick={() => { }} />);
      const cells = component.find(TableCell);
      expect(cells.get(0).props.className).not.toContain('th-up');
      expect(cells.get(0).props.className).not.toContain('th-down');
      expect(cells.get(1).props.className).toContain('th-up');
      expect(cells.get(1).props.className).not.toContain('th-down');
      expect(cells.get(2).props.className).not.toContain('th-up');
      expect(cells.get(2).props.className).not.toContain('th-down');
    });
    it('should select revert rate th if reported in sortInfo', () => {
      component = shallow(<CurrencyTableHead sortInfo={{ key: 'rate', reverted: true }} onThClick={() => { }} />);
      const cells = component.find(TableCell);
      expect(cells.get(0).props.className).not.toContain('th-up');
      expect(cells.get(0).props.className).not.toContain('th-down');
      expect(cells.get(1).props.className).not.toContain('th-up');
      expect(cells.get(1).props.className).toContain('th-down');
      expect(cells.get(2).props.className).not.toContain('th-up');
      expect(cells.get(2).props.className).not.toContain('th-down');
    });

    it('should select variation th if reported in sortInfo', () => {
      component = shallow(<CurrencyTableHead sortInfo={{ key: 'variation', reverted: false }} onThClick={() => { }} />);
      const cells = component.find(TableCell);
      expect(cells.get(0).props.className).not.toContain('th-up');
      expect(cells.get(0).props.className).not.toContain('th-down');
      expect(cells.get(1).props.className).not.toContain('th-up');
      expect(cells.get(1).props.className).not.toContain('th-down');
      expect(cells.get(2).props.className).toContain('th-up');
      expect(cells.get(2).props.className).not.toContain('th-down');
    });
    it('should select revert variation th if reported in sortInfo', () => {
      component = shallow(<CurrencyTableHead sortInfo={{ key: 'variation', reverted: true }} onThClick={() => { }} />);
      const cells = component.find(TableCell);
      expect(cells.get(0).props.className).not.toContain('th-up');
      expect(cells.get(0).props.className).not.toContain('th-down');
      expect(cells.get(1).props.className).not.toContain('th-up');
      expect(cells.get(1).props.className).not.toContain('th-down');
      expect(cells.get(2).props.className).not.toContain('th-up');
      expect(cells.get(2).props.className).toContain('th-down');
    });
  });

  describe('events', () => {
    let onThClick;
    let component;
    beforeEach(() => {
      onThClick = jest.fn();
      component = shallow(<CurrencyTableHead sortInfo={{ key: 'variation', reverted: true }} onThClick={onThClick} />);
    });
    it('should call the onThClick and pass `currency` string when user clicks on currency th', () => {
      component.find('.currency-table-head__th-currency').simulate('click');
      expect(onThClick).toHaveBeenCalledWith('currency');
    });

    it('should call the onThClick and pass `rate` string when user clicks on rate th', () => {
      component.find('.currency-table-head__th-rate').simulate('click');
      expect(onThClick).toHaveBeenCalledWith('rate');
    });

    it('should call the onThClick and pass `variation` string when user clicks on variation th', () => {
      component.find('.currency-table-head__th-variation').simulate('click');
      expect(onThClick).toHaveBeenCalledWith('variation');
    });
  });

});
