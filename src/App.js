import { useState, useMemo } from 'react';
import './App.css';
import originalData from './currencies.json';
import { Container, Tabs, Tab, Typography, Divider } from '@mui/material';
import { CurrencyTable } from './components/CurrencyTable/CurrencyTable';

function App() {
  const data = useMemo(() => Object.entries(originalData).map(([date, info]) => ({
    ...info,
    date
  })), []);
  const [selectedDateIndex, setSelectedDateIndex] = useState(0);

  const { currencyCurrentDate, currencyPrevDate } = useMemo(() => {
    return {
      currencyCurrentDate: data[selectedDateIndex],
      currencyPrevDate: selectedDateIndex > 0 ? data[selectedDateIndex - 1] : null
    };
  }, [selectedDateIndex, data]);

  return (
    <Container fixed>
      <header>
        <Typography className="title">CURRENCY EXCHANGE</Typography>
      </header>
      <Tabs value={selectedDateIndex} onChange={(_, index) => setSelectedDateIndex(index)}>
        {data.map(({ date }) => <Tab key={date} label={date} />)}
      </Tabs>
      <Divider />
      <CurrencyTable currencyPrevDate={currencyPrevDate?.rates} currencyCurrentDate={currencyCurrentDate.rates} />
    </Container>
  );
}

export default App;
