import { shallow } from 'enzyme';
import { Tabs, Tab } from '@mui/material';
import { CurrencyTable } from './components/CurrencyTable/CurrencyTable';
import originalData from './currencies.json';
import App from './App';

describe('<App />', () => {
  let appComponent;
  beforeEach(() => {
    appComponent = shallow(<App />);
  });

  it('should have the correct title', () => {
    expect(appComponent.find('header').text()).toBe('CURRENCY EXCHANGE');
  });

  it('should pass the correct amount of tabs', () => {
    expect(appComponent.find(Tab).length).toBe(10);
  });

  it('should show the correct labels for the dates', () => {
    const keys = Object.keys(originalData);
    appComponent.find(Tab).forEach((tab, keyId) => {
      expect(tab.props().label).toBe(keys[keyId]);
    });
  });

  describe('CurrencyTable interaction', () => {
    let tabs;
    let currencyTable;
    let data;
    beforeEach(() => {
      tabs = appComponent.find(Tabs);

      data = Object.entries(originalData).map(([date, info]) => ({
        ...info,
        date
      }));
    });

    it('should have default data', () => {
      currencyTable = appComponent.find(CurrencyTable);
      const { currencyPrevDate, currencyCurrentDate } = currencyTable.props();
      expect(currencyPrevDate).toBe(undefined);
      expect(currencyCurrentDate).toEqual(data[0].rates);
    });

    it('should pass the correct dates object once user clicks on tab', () => {
      const selectedDateIndex = 4;
      const expectedCurrentData = data[selectedDateIndex].rates;
      const expectedPrevData = data[selectedDateIndex - 1].rates;

      tabs.simulate('change', {}, selectedDateIndex);
      currencyTable = appComponent.find(CurrencyTable);
      const { currencyPrevDate, currencyCurrentDate } = currencyTable.props();

      expect(currencyPrevDate).toEqual(expectedPrevData);
      expect(currencyCurrentDate).toEqual(expectedCurrentData);
    });
  });
});

