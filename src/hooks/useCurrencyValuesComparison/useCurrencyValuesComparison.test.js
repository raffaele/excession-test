import { renderHook, act } from '@testing-library/react-hooks';
import { useCurrencyValuesComparison } from "./useCurrencyValuesComparison";

describe('useCurrencyValuesComparison hook', () => {
  const currentRates = {
    USD: 25,
    CAD: 8,
    EUR: 20
  };
  it('should format the array based on the current-date currency when previous one is null', () => {
    const { result } = renderHook(() => useCurrencyValuesComparison(currentRates, null));
    expect(result.current.fullCurrenciesInfo).toEqual([
      { currency: 'USD', rate: 25, variation: null },
      { currency: 'CAD', rate: 8, variation: null },
      { currency: 'EUR', rate: 20, variation: null }
    ]);
  });

  it('should format the array based on the currencies evaluation of current and previous day', () => {
    const ratePreviousDate = {
      USD: 20,
      CAD: 10,
      EUR: 20
    };
    const { result } = renderHook(() => useCurrencyValuesComparison(currentRates, ratePreviousDate));
    expect(result.current.fullCurrenciesInfo).toEqual([
      { currency: 'USD', rate: 25, variation: 25 },
      { currency: 'CAD', rate: 8, variation: -20 },
      { currency: 'EUR', rate: 20, variation: 0 }
    ]);
  });
});