import { useMemo, useCallback } from 'react';

export function useCurrencyValuesComparison(currentRates, previousRates) {
  const getPercentageDiff = useCallback((currency, rate) => {
    if (!previousRates) return null;
    const prevRate = previousRates[currency];
    return 100 * (rate - prevRate) / prevRate;
  }, [previousRates]);

  const fullCurrenciesInfo = useMemo(() => Object.entries(currentRates).map(([currency, rate]) => {
    return {
      currency,
      rate,
      variation: getPercentageDiff(currency, rate)
    };

  }), [currentRates, getPercentageDiff]);

  return {
    fullCurrenciesInfo,
  };
}