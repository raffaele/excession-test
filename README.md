## The task

Create a web page to show the currency data in `currencies.json`. You should display currency, exchange rate and percentage change from the previous day. The user should be able to sort by each of these. The currencies with the largest positive and negative rate changes since the previous day should be visually distinguished.

The solution should be written in modern Javascript (ES6 onwards), outside of that though feel free to use any Javascript libraries or frameworks you want to.

To give a bit more insight, what we're looking for with this task is:

- Has the task brief been fulfilled? Are all the requirements that were asked for included in the solution?

- A well structured codebase that's easy to read. Could someone else pick up and work with it?

- Evidence that you're comfortable working with the technology you've used. If you've not used the same tech stack as us that's not a problem - we're looking for aptitude.

- A good familiarity with CSS, plus if you have UI design skills that's a bonus!

- Unit tests which prove that your solution works and meets the requirements above.
